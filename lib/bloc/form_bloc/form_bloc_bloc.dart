import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:self_checkout_app/models/user.dart';

import 'package:self_checkout_app/services/user_repository.dart';
import './bloc.dart';

class FormBlocBloc extends Bloc<FormBlocEvent, FormBlocState> {
  @override
  FormBlocState get initialState => FormBlocState.empty();

  @override
  Stream<FormBlocState> mapEventToState(
    FormBlocEvent event,
  ) async* {
    if (event is EditUserimage) {
      yield* _mapEditUserImageToState(event.user);
    } else if (event is EditUserDetails) {
      yield* _mapEditUserDetailsToState(event.user);
    }
  }

  // Edit User Images

  Stream<FormBlocState> _mapEditUserImageToState(UserModel user) async* {
    yield FormBlocState.loading();
    try {
      final UserRepository userRepository = UserRepository();

      yield FormBlocState.loading();
      await userRepository.updateImage(user);
      yield FormBlocState.success();
    } catch (e) {
      print("Error adding brand, ${e.message}");
      yield FormBlocState.failure("Error ading brand");
    }
  }

  // Edit User details

  Stream<FormBlocState> _mapEditUserDetailsToState(UserModel user) async* {
    yield FormBlocState.loading();
    try {
      final UserRepository userRepository = UserRepository();

      yield FormBlocState.loading();
      await userRepository.updateUseDetails(user);
      yield FormBlocState.success();
    } catch (e) {
      print("Error adding brand, ${e.message}");
      yield FormBlocState.failure("Error ading brand");
    }
  }
}
