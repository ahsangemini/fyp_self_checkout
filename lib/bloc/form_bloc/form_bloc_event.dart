import 'package:equatable/equatable.dart';
import 'package:self_checkout_app/models/user.dart';
import 'package:meta/meta.dart';

abstract class FormBlocEvent extends Equatable {
  const FormBlocEvent();
}

class EditUserimage extends FormBlocEvent {
  final UserModel user;
  EditUserimage({@required this.user}) : assert(user != null);

  @override
  List<Object> get props => [user];
}

class EditUserDetails extends FormBlocEvent {
  final UserModel user;
  EditUserDetails({@required this.user}) : assert(user != null);

  @override
  List<Object> get props => [user];
}

class DeleteAddress extends FormBlocEvent {
  final String addressId;
  DeleteAddress({@required this.addressId}) : assert(addressId != null);

  @override
  List<Object> get props => [addressId];
}
