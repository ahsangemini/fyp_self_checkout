const Settings = {
  "Setting": {
    "MainColor": "#101097",
    "MainDarkColor": "#1b2a49",
    "SecondaryColor": "#6CC500",
    "SecondaryDarkColor": "#fffdf9",
    "TeritiaryColor": "#F32850"
  }
};

const Logo = {
  "title": "SelfCheckout",
  "fontSize": 24.0,
  "fontFamily": "Fashion",
  "isImage": false,
  "isAsset": false,
  "image":
      "https://s3-eu-west-1.amazonaws.com/cdn1.mullenlowegroup.com/uploads/sites/43/2016/06/flipkart-logo-2.jpg" // ----> If isImage true then add image URL, Also make isAsset true if you have kept your image in Asset File.
};

const StripeConfig = {
  "publishableKey":
      "pk_test_51H1WT4D4PzUjaBm1yZ45cIPHLTaHnxB7RGYiXYitoqTVr5GcaiJZfL1F4KY7mLqgqp0Mnph6BO0LKIqWqyfKpvVQ00Vo9uQuKr",
  "merchantId": "Test",
  "androidPayMode": 'test',
  "enabled": true,
};

const PaymentProviderEnabled = {"name": "stripe"};

const CurrencyFormat = {
  "en": {"symbol": "\$", "decimalDigits": 2},
  "ur": {"symbol": "Rs", "decimalDigits": 2},
};
