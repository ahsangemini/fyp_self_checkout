import 'package:cloud_functions/cloud_functions.dart';
import 'package:self_checkout_app/models/cart.dart';

class CloudService {
  static Future<HttpsCallableResult> createPaymentIntent({
    List<CartModel> cart,
    double totalAmount,
    double taxAmount,
    double subTotal,
    String currency,
    String customer,
  }) async =>
      await CloudFunctions.instance
          .getHttpsCallable(functionName: 'createPaymentIntent')
          .call(<String, dynamic>{
        'cart': cart.map((c) => c.toMap()).toList(),
        'totalAmount': totalAmount.toStringAsPrecision(4),
        'taxAmount': taxAmount,
        'subTotal': subTotal,
        'currency': 'usd',
        'customer': customer
      });
}
