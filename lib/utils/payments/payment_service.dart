import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:stripe_payment/stripe_payment.dart';

class PaymentService {
  Future<void> addCard(PaymentMethod method, uid) async {
    await Firestore.instance
        .collection('users')
        .document(uid)
        .collection('payment_methods')
        .add(method.toJson());
  }
}
