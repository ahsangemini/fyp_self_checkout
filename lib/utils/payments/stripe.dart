import 'dart:collection';
import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:self_checkout_app/bloc/orders_bloc/bloc.dart';
import 'package:self_checkout_app/models/cart.dart';
import 'package:self_checkout_app/models/user.dart';
import 'package:self_checkout_app/utils/config.dart';
import 'package:self_checkout_app/widgets/custom_route.dart';
import 'package:self_checkout_app/widgets/orders/orders.dart';
import 'package:stripe_payment/stripe_payment.dart';
import 'package:fluttertoast/fluttertoast.dart';

class StripePaymentMethod extends StatefulWidget {
  final List<CartModel> cart;
  final double totalAmount, taxAmount, subTotal;
  final UserModel user;
  final Function onFinish;

  StripePaymentMethod(
      {Key key,
      @required this.cart,
      @required this.totalAmount,
      @required this.subTotal,
      @required this.user,
      @required this.onFinish,
      @required this.taxAmount})
      : super(key: key);

  @override
  _StripePaymentMethodState createState() => _StripePaymentMethodState();
}

class _StripePaymentMethodState extends State<StripePaymentMethod> {
  @override
  initState() {
    super.initState();

    StripePayment.setOptions(StripeOptions(
        publishableKey: StripeConfig["publishableKey"],
        merchantId: StripeConfig["merchantId"],
        androidPayMode: StripeConfig["androidPayMode"]));

    Future.delayed(Duration.zero, () {
      StripePayment.paymentRequestWithCardForm(CardFormPaymentRequest(
        requiredBillingAddressFields: "",
      )).then((paymentMethod) async {
        log(paymentMethod.toString());
        Firestore.instance
            .collection('users/${widget.user.uid}/payment_methods')
            .add(paymentMethod.toJson());

        Firestore.instance.collection('users/${widget.user.uid}/payments').add({
          'cart': widget.cart.map((c) => c.toMap()).toList(),
          'totalAmount': widget.totalAmount,
          'taxAmount': widget.taxAmount,
          'subTotal': widget.subTotal,
          'currency': 'usd',
          'payment_method': paymentMethod.toJson(),
        });

        Future.delayed(Duration(seconds: 2), () {
          Navigator.of(context).pushReplacement(
            FadeRoute(
              page: BlocProvider(
                create: (context) =>
                    OrdersBloc()..add(LoadOrders(userId: widget.user.uid)),
                child: Orders(
                  user: widget.user,
                ),
              ),
            ),
          );
        });

        // final HttpsCallable callable = CloudFunctions.instance.getHttpsCallable(
        //   functionName: 'createStripeChareg',
        // );

        // try {
        //   final HttpsCallableResult resp =
        //       await callable.call(<String, dynamic>{
        //     'cart': widget.cart.map((c) => c.toMap()).toList(),
        //     'totalAmount': widget.totalAmount,
        //     'taxAmount': widget.taxAmount,
        //     'subTotal': widget.subTotal
        //   });

        //   print(resp.data);
        //   StripePayment.confirmPaymentIntent(PaymentIntent(
        //     clientSecret: resp.data['client_secret'],
        //     paymentMethodId: paymentMethod.id,
        //   )).then((data) {
        //     print(data.status);
        //     widget.onFinish(data);
        //   }).catchError(setError);
        // } catch (e) {
        //   print(e.message);

        //   setError(e);
        // }
      }).catchError(setError);
    });
  }

  void setError(dynamic error) {
    print(error.code);
    if (error.code == "cancelled") {
      Fluttertoast.showToast(
          msg: "${error.message.toString()}",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
      Navigator.pop(context);
      return;
    }
    Fluttertoast.showToast(
        msg: "${error.message.toString()}",
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: Theme.of(context).colorScheme.background,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            }),
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0.0,
      ),
      body: Container(
          child: Center(
        child: CircularProgressIndicator(),
      )),
    );
  }
}
