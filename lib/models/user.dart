import 'package:cloud_firestore/cloud_firestore.dart';

class UserModel {
  final String uid, avatarURL, displayName, email, type, role, customer_id;
  UserModel(
      {this.uid,
      this.avatarURL,
      this.displayName,
      this.email,
      this.type,
      this.customer_id,
      this.role});

  factory UserModel.fromFirestore(DocumentSnapshot doc) {
    Map data = doc.data;
    return UserModel(
      uid: doc.documentID,
      avatarURL: data['avatarURL'],
      displayName: data['displayName'] != null ? data['displayName'] : "User",
      email: data['email'] != null ? data["email"] : "email@email.com",
      type: data['type'],
      customer_id: data['customer_id'],
      role: data['role'],
    );
  }
}
