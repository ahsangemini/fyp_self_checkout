import 'package:flutter/material.dart';
import 'package:self_checkout_app/main.dart';
import 'package:self_checkout_app/services/user_repository.dart';
import 'package:self_checkout_app/utils/config.dart';
import 'package:self_checkout_app/utils/tools.dart';
import 'package:self_checkout_app/widgets/custom_route.dart';
import 'package:self_checkout_app/widgets/onboarding/onboarding.dart';
import 'package:self_checkout_app/widgets/onboarding/page_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

var color = Settings['Setting']['MainColor'];

final pageList = [
  PageModel(
    color: HexColor(color),
    heroAssetPath: 'assets/intro/vendor.svg',
    title: Text('Ahsan 62715',
        style: TextStyle(
          fontWeight: FontWeight.w800,
          color: Colors.white,
          fontSize: 34.0,
        )),
    body: Text(
        'Self-Checkout App made by Syed Ahsan Ali (MC-3-17-62715) for the purpose of FYP',
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Colors.white,
          fontSize: 18.0,
        )),
    iconAssetPath: 'assets/intro/vendor.svg',
  ),
  PageModel(
      color: HexColor(color),
      heroAssetPath: 'assets/intro/cart.svg',
      title: Text('Queue',
          style: TextStyle(
            fontWeight: FontWeight.w800,
            color: Colors.white,
            fontSize: 34.0,
          )),
      body: Text('No need to stand in queue',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
            fontSize: 18.0,
          )),
      iconAssetPath: 'assets/intro/cart.svg'),
  PageModel(
      color: HexColor(color),
      heroAssetPath: 'assets/intro/scan.png',
      title: Text('Scan',
          style: TextStyle(
            fontWeight: FontWeight.w800,
            color: Colors.white,
            fontSize: 34.0,
          )),
      body: Text('Pick the product and scan it no need of que',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
            fontSize: 18.0,
          )),
      iconAssetPath: 'assets/intro/scan.png'),
  PageModel(
    color: HexColor(color),
    heroAssetPath: 'assets/intro/payment.svg',
    title: Text('Payment',
        style: TextStyle(
          fontWeight: FontWeight.w800,
          color: Colors.white,
          fontSize: 34.0,
        )),
    body: Text('Make payment from app or go to the counter to pay your choice',
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Colors.white,
          fontSize: 18.0,
        )),
    iconAssetPath: 'assets/intro/payment.svg',
  ),
];

class IntroScreen extends StatelessWidget {
  const IntroScreen({Key key}) : super(key: key);

  void gotoScreen(context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    await prefs.setBool('seen', true);
    final UserRepository userRepository = UserRepository();
    Navigator.of(context).pushReplacement(FadeRoute(
        page: App(
      userRepository: userRepository,
    )));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: HtOnBoarding(
        doneButtonText: "Done",
        skipButtonText: "Skip",
        pageList: pageList,
        onDoneButtonPressed: () {
          gotoScreen(context);
        },
        onSkipButtonPressed: () {
          gotoScreen(context);
        },
      ),
    );
  }
}
