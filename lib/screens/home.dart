import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:self_checkout_app/admin/widgets/drawer.dart';
import 'package:self_checkout_app/bloc/cart_bloc/bloc.dart';
import 'package:self_checkout_app/models/cart.dart';
import 'package:self_checkout_app/models/product.dart';
import 'package:self_checkout_app/models/user.dart';
import 'package:self_checkout_app/screens/account.dart';
import 'package:self_checkout_app/screens/cart_screen.dart';
import 'package:self_checkout_app/screens/category_screen.dart';
import 'package:self_checkout_app/screens/home_screen.dart';
import 'package:self_checkout_app/screens/search_screen.dart';
import 'package:self_checkout_app/widgets/appbar.dart';
import 'package:self_checkout_app/widgets/barcode_reader.dart';
import 'package:self_checkout_app/widgets/products/product_details.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';

class Home extends StatefulWidget {
  final UserModel user;

  const Home({Key key, @required this.user}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return HomeState();
  }
}

class HomeState extends State<Home> {
  PageController _pageController;
  int pageIndex = 0;

  Future<void> _openScanBarcode() async {
    await Permission.camera.request();
    if (await Permission.camera.isGranted) {
      Map barcodeValueMap = await Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => BarcodeReader(),
            fullscreenDialog: true,
          ));
      if (barcodeValueMap != null) {
        String barcodeValue = barcodeValueMap['vector'];
        //print(context.read<ScanProvider>().scanResult.rawContent);

        ProductModel product = ProductModel.fromFirestore(await Firestore
            .instance
            .collection("products")
            .document(barcodeValue)
            .get());
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (BuildContext context) {
              return ProductDetails(
                product: product,
                userId: widget.user.uid,
              );
            },
          ),
        );
      }
    }
  }

  Future<void> _onPressedBarcode() async {
    try {
      await _openScanBarcode();
    } catch (e) {
      throw e;
    }
  }

  void _onChangePageIndex(index) {
    setState(() {
      pageIndex = index;
    });
    _pageController.jumpToPage(index);
  }

  @override
  Widget build(BuildContext context) {
    bool isAdmin = widget.user.role == 'admin' ? true : false;
    return Scaffold(
      appBar: new PreferredSize(
        preferredSize: Size.fromHeight(58.0), // Change the height of the appbar
        child: CustomAppbar(),
      ), //AppBar ,
      body: PageView(
        physics: NeverScrollableScrollPhysics(),
        controller: _pageController,
        onPageChanged: _onChangePageIndex,
        children: [
          HomeScreen(
            userId: widget.user.uid,
          ),
          CategoryScreen(
            userId: widget.user.uid,
          ),
          CartScreen(
            user: widget.user,
          ),
          SearchScreen(
            userId: widget.user.uid,
          ),
          AccountScreen(),
        ],
      ),
      drawer: isAdmin ? CustomDrawer() : null,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton:
          //TODO see if the floating action button for scaning will work if it does then remove the context of the FAB blow and keep the above FAB

          Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            height: 65.0,
            width: 65.0,
            child: FittedBox(
              child: FloatingActionButton(
                  backgroundColor: Theme.of(context).primaryColor,
                  onPressed: () {
                    _onChangePageIndex(2);
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Image.asset(
                        "assets/icons/icon-cart.png",
                        fit: BoxFit.contain,
                        width: 20,
                        height: 20,
                        color: Colors.white,
                      ),
                      BlocProvider(
                        create: (context) =>
                            CartBloc()..add(LoadCart(userId: widget.user.uid)),
                        child: Container(
                          child: BlocBuilder<CartBloc, CartState>(
                            builder: (BuildContext context, CartState state) {
                              if (state is CartLoading) {
                                return Center();
                              }
                              if (state is CartLoaded) {
                                return StreamBuilder(
                                  stream: state.cart,
                                  initialData: [],
                                  builder: (BuildContext context,
                                      AsyncSnapshot snapshot) {
                                    if (snapshot.hasError) {
                                      return Container(
                                        width: 20,
                                        height: 20,
                                        child: Center(
                                          child: Text("Something went wrong"),
                                        ),
                                      );
                                    }
                                    switch (snapshot.connectionState) {
                                      case ConnectionState.waiting:
                                        return Container(
                                          width: 20,
                                          height: 20,
                                          child: Center(
                                            child: Text(
                                              "0",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                        );
                                        break;
                                      default:
                                        final List<CartModel> carts =
                                            snapshot.data;
                                        print(carts.length);
                                        return Container(
                                          width: 20,
                                          height: 20,
                                          child: Center(
                                            child: Text(
                                              carts.length.toString(),
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                        );
                                    }
                                  },
                                );
                              }
                              return Container(
                                width: 20,
                                height: 20,
                                child: Text(
                                  "0",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                    ],
                  )
                  // elevation: 5.0,
                  ),
            ),
          ),
          Container(
            height: 65.0,
            width: 65.0,
            child: FittedBox(
              child: RaisedButton(
                shape:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
                onPressed: () {
                  _onPressedBarcode();
                },
                child: Icon(Icons.camera, color: Colors.white),
                // elevation: 5.0,
              ),
            ),
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        elevation: 3.0,
        items: [
          BottomNavigationBarItem(
              icon: Image.asset(
                "assets/icons/icon-home.png",
                fit: BoxFit.contain,
                width: 20,
                height: 20,
                color: pageIndex == 0
                    ? Theme.of(context).iconTheme.color
                    : Theme.of(context).hintColor,
              ),
              title: Container(height: 0.0)),
          BottomNavigationBarItem(
              icon: Image.asset(
                "assets/icons/icon-category.png",
                fit: BoxFit.contain,
                width: 20,
                height: 20,
                color: pageIndex == 1
                    ? Theme.of(context).iconTheme.color
                    : Theme.of(context).hintColor,
              ),
              title: Container(height: 0.0)),
          BottomNavigationBarItem(
              title: Container(
                height: 0,
              ),
              icon: Icon(
                Icons.category,
                color: Colors.transparent,
              )),
          BottomNavigationBarItem(
              icon: Image.asset("assets/icons/icon-search.png",
                  fit: BoxFit.contain,
                  width: 20,
                  height: 20,
                  color: pageIndex == 3
                      ? Theme.of(context).iconTheme.color
                      : Theme.of(context).hintColor),
              title: Container(height: 0.0)),
          BottomNavigationBarItem(
            icon: Image.asset("assets/icons/icon-user.png",
                fit: BoxFit.contain,
                width: 20,
                height: 20,
                color: pageIndex == 4
                    ? Theme.of(context).iconTheme.color
                    : Theme.of(context).hintColor),
            title: Container(height: 0.0),
          ),
        ],
        onTap: _onChangePageIndex,
        currentIndex: pageIndex,
        fixedColor: Theme.of(context).primaryColor,
        type: BottomNavigationBarType.fixed,
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }
}
