import 'package:flutter/material.dart';
import 'package:self_checkout_app/utils/config.dart';

class SplashCreen extends StatelessWidget {
  const SplashCreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: null,
      body: Container(
        decoration: BoxDecoration(color: Theme.of(context).primaryColor),
        child: Center(
            child: Text(
          "Ahsan 62715",
          textAlign: TextAlign.center,
          style: new TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            letterSpacing: 0.73,
            fontSize: 24,
          ),
        )),
      ),
    );
  }
}
