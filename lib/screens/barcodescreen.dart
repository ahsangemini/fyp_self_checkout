import 'dart:io';
import 'dart:typed_data';
import 'dart:math';
import 'package:barcode/barcode.dart';
import 'package:barcode_widget/barcode_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:path_provider/path_provider.dart';

class BarCodePage extends StatefulWidget {
  const BarCodePage({Key key, @required this.data}) : super(key: key);
  final String data;

  @override
  _BarCodePageState createState() => _BarCodePageState();
}

class _BarCodePageState extends State<BarCodePage> {
  @override
  void initState() {
    super.initState();
    random = new Random();
  }

  Random random;
  void getBarcode() async {}
  Future<File> get _localFile async {
    final path = await _localPath;
    int randomNumber = random.nextInt(100);

    return File('$path/barcode${randomNumber}.svg');
  }

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<String> buildBarcode(
    Barcode bc,
    String data, {
    String filename,
    double width,
    double height,
  }) async {
    /// Create the Barcode
    final svg = bc.toSvg(data, width: width ?? 200, height: height ?? 80);
    // Save the image
    final barcodeFile = await _localFile;
    barcodeFile.writeAsStringSync(svg);
    return barcodeFile.path;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
          onPressed: () => Navigator.pop(context),
        ),
        title: const Text('Your Barcode'),
      ),
      body: Center(
        child: Container(
          width: MediaQuery.of(context).size.width * 0.9,
          height: MediaQuery.of(context).size.height * 0.3,
          child: BarcodeWidget(
            data: '6qKaAGazt6u7SfmiaB9N',
            barcode: Barcode.code128(),
          ),
        ),
      ),
    );
  }
}
