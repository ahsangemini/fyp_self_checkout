import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:self_checkout_app/admin/screens/brands.dart';
import 'package:self_checkout_app/admin/screens/category_admin.dart';
import 'package:self_checkout_app/admin/screens/orders.dart';
import 'package:self_checkout_app/admin/screens/products.dart';
import 'package:self_checkout_app/admin/screens/slider_admin.dart';
import 'package:self_checkout_app/admin/screens/users.dart';
import 'package:self_checkout_app/authentication_bloc/bloc.dart';
import 'package:self_checkout_app/utils/config.dart';
import 'package:self_checkout_app/widgets/custom_route.dart';
import 'package:self_checkout_app/widgets/text_widget.dart';

List<Map> navItems = [
  {"icon": "stores", "label": "STORES"},
  {"icon": "orders", "label": "ORDERS"},
  {"icon": "referrals", "label": "REFERRALS"},
  {"icon": "support", "label": "SUPPORT"},
  {"icon": "person", "label": "ACCOUNT"},
];

class CustomDrawer extends StatelessWidget {
  const CustomDrawer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: new Material(
        color: Theme.of(context).primaryColor,
        child: new Container(
          padding: EdgeInsets.symmetric(vertical: 48.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 30,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 16.0, right: 16),
                child: Text(
                  "Ahsan 62715",
                  textAlign: TextAlign.center,
                  style: new TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 0.73,
                    fontSize: 24,
                  ),
                ),
              ),
              SizedBox(
                height: 16,
              ),
              Divider(
                color: Colors.white,
              ),
              SizedBox(
                height: 16,
              ),
              ListTile(
                onTap: () {
                  // Navigator.pop(context);
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => UsersAdmin()));
                },
                leading: ImageIcon(
                  AssetImage("assets/icons/users.png"),
                  size: 24.0,
                  color: Colors.white,
                ),
                title: CustomTextWidget(
                    textKey: "users",
                    style: new TextStyle(
                        color: Colors.white,
                        fontFamily: 'SF',
                        fontWeight: FontWeight.normal,
                        fontSize: 24.0)),
              ),
              ListTile(
                onTap: () {
                  // Navigator.pop(context);
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => ProductsAdmin()));
                },
                leading: ImageIcon(
                  AssetImage("assets/icons/products.png"),
                  size: 24.0,
                  color: Colors.white,
                ),
                title: CustomTextWidget(
                    textKey: "products",
                    style: new TextStyle(
                        color: Colors.white,
                        fontFamily: 'SF',
                        fontWeight: FontWeight.normal,
                        fontSize: 24.0)),
              ),
              ListTile(
                onTap: () {
                  Navigator.of(context).push(FadeRoute(page: AdminOrders()));
                },
                leading: ImageIcon(
                  AssetImage("assets/icons/orders.png"),
                  size: 24.0,
                  color: Colors.white,
                ),
                title: CustomTextWidget(
                    textKey: "orders",
                    style: new TextStyle(
                        color: Colors.white,
                        fontFamily: 'SF',
                        fontWeight: FontWeight.normal,
                        fontSize: 24.0)),
              ),
              ListTile(
                onTap: () {
                  Navigator.of(context).push(FadeRoute(page: CategoryAdmin()));
                },
                leading: ImageIcon(
                  AssetImage("assets/icons/category.png"),
                  size: 24.0,
                  color: Colors.white,
                ),
                title: CustomTextWidget(
                    textKey: "category",
                    style: new TextStyle(
                        color: Colors.white,
                        fontFamily: 'SF',
                        fontWeight: FontWeight.normal,
                        fontSize: 24.0)),
              ),
              ListTile(
                onTap: () {
                  Navigator.of(context).push(FadeRoute(page: BrandsAdmin()));
                },
                leading: ImageIcon(
                  AssetImage("assets/icons/brands.png"),
                  size: 24.0,
                  color: Colors.white,
                ),
                title: CustomTextWidget(
                    textKey: "brands",
                    style: new TextStyle(
                        color: Colors.white,
                        fontFamily: 'SF',
                        fontWeight: FontWeight.normal,
                        fontSize: 24.0)),
              ),
              ListTile(
                onTap: () {
                  Navigator.of(context).push(FadeRoute(page: SlidersAdmin()));
                },
                leading: ImageIcon(
                  AssetImage("assets/icons/tag.png"),
                  size: 24.0,
                  color: Colors.white,
                ),
                title: CustomTextWidget(
                    textKey: "slider",
                    style: new TextStyle(
                        color: Colors.white,
                        fontFamily: 'SF',
                        fontWeight: FontWeight.normal,
                        fontSize: 24.0)),
              ),
              Spacer(),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Divider(
                    color: Colors.white,
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  InkWell(
                    onTap: () {
                      BlocProvider.of<AuthenticationBloc>(context).add(
                        LoggedOut(),
                      );
                      Navigator.of(context).pop();
                    },
                    child: ListTile(
                      leading: Icon(
                        Icons.exit_to_app,
                        size: 24.0,
                        color: Colors.white,
                      ),
                      title: CustomTextWidget(
                          textKey: "signout",
                          style: new TextStyle(
                              color: Colors.white,
                              fontFamily: 'SF',
                              fontWeight: FontWeight.normal,
                              fontSize: 24.0)),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
