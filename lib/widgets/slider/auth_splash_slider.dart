import 'package:flutter/material.dart';
import 'package:self_checkout_app/utils/tools.dart';
import 'package:self_checkout_app/widgets/base_widget.dart';

import 'carousel_slider.dart';

final List<String> imgList = [
  "https://firebasestorage.googleapis.com/v0/b/fyp-self-checkout-store.appspot.com/o/initialphotos%2Fqmobile.jpg?alt=media&token=6ce6c93a-7014-4031-a70c-d20df061c3df",
  "https://firebasestorage.googleapis.com/v0/b/fyp-self-checkout-store.appspot.com/o/initialphotos%2Frice.jpeg?alt=media&token=6bb8bc40-a88d-4fb5-8538-5ce1e2eaf2a9",
  "https://firebasestorage.googleapis.com/v0/b/fyp-self-checkout-store.appspot.com/o/initialphotos%2Fqmobile.jpg?alt=media&token=6ce6c93a-7014-4031-a70c-d20df061c3df",
  "https://firebasestorage.googleapis.com/v0/b/fyp-self-checkout-store.appspot.com/o/initialphotos%2Folpers.jpg?alt=media&token=171ec589-caa2-4123-87ec-4552babda95b",
  "https://firebasestorage.googleapis.com/v0/b/fyp-self-checkout-store.appspot.com/o/initialphotos%2Fimtiaz.png?alt=media&token=fee552d7-e310-492b-b46f-83d150ce991c"
];

final List child = map<Widget>(
  imgList,
  (index, i) {
    return Container(
      margin: EdgeInsets.only(top: 12.0, bottom: 4.0, left: 8.0, right: 8.0),
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(0.0)),
        child: Stack(children: <Widget>[
          Tools.image(
            url: i,
            fit: BoxFit.cover,
            width: double.maxFinite,
            height: 300.0,
          ),
        ]),
      ),
    );
  },
).toList();

List<T> map<T>(List list, Function handler) {
  List<T> result = [];
  for (var i = 0; i < list.length; i++) {
    result.add(handler(i, list[i]));
  }

  return result;
}

class CarouselWithIndicator extends StatefulWidget {
  @override
  _CarouselWithIndicatorState createState() => _CarouselWithIndicatorState();
}

class _CarouselWithIndicatorState extends State<CarouselWithIndicator> {
  int _current = 0;

  Widget buildContent(double height) {
    return CarouselSlider(
      height: height,
      items: child,
      autoPlay: true,
      autoPlayInterval: Duration(seconds: 5),
      enlargeCenterPage: true,
      aspectRatio: 2.0,
      autoPlayCurve: Curves.linear,
      viewportFraction: 0.8,
      pauseAutoPlayOnTouch: Duration(seconds: 1),
      onPageChanged: (index) {
        setState(() {
          _current = index;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    double heightQuery = MediaQuery.of(context).size.height;
    return BaseWidget(builder: (context, sizeInfo) {
      if (sizeInfo.orientation == Orientation.portrait) {
        if (sizeInfo.deviceScreenType == DeviceScreenType.Mobile) {
          return buildContent(heightQuery / 4);
        } else {
          return buildContent(heightQuery / 4);
        }
      } else {
        if (sizeInfo.deviceScreenType == DeviceScreenType.Mobile) {
          return buildContent(heightQuery / 2);
        } else {
          return buildContent(heightQuery / 2);
        }
      }
    });
  }
}
