import 'dart:developer';
import 'dart:io';

import 'package:self_checkout_app/utils/barcode_box.dart';
import 'package:self_checkout_app/utils/detection_utils.dart';
import 'package:path_provider/path_provider.dart';
import 'package:camera/camera.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:uuid/uuid.dart';

class BarcodeReader extends StatefulWidget {
  @override
  BarcodeReaderState createState() => BarcodeReaderState();
}

class BarcodeReaderState extends State<BarcodeReader> {
  CameraController _camera;
  bool _isDetecting = false;
  CameraLensDirection _direction = CameraLensDirection.back;
  double threshold = 1.0;
  List e1;
  bool _barcodeFound = false;
  bool _multipleBarcodes = false;
  String res;
  bool _buttonEnabled = true;
  List<Barcode> _barcodes = [];
  Uuid uuid = Uuid();

  @override
  void initState() {
    super.initState();

    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    try {
      log("Start");
      _initializeCamera();
      log("End");
    } catch (e) {
      log(e.toString());
    }
  }

  void _initializeCamera() async {
    try {
      CameraDescription description = await getCamera(_direction);

      _camera = CameraController(description, ResolutionPreset.max,
          enableAudio: false);
      await _camera.initialize();
      setState(() {});
      _camera.startImageStream((CameraImage image) {
        // the detector starts from here
        if (_camera != null) {
          setState(() {});
          if (_isDetecting)
            return; // if it's already detecting then it should be done (return) and move forward
          _isDetecting = true; // otherwise, we start detecting
          detect(
                  // here we detect methods, its simply the firebase that we give the
                  // fases we have been made, the detect is calculating the faces
                  image: image,
                  detectInImage: _getDetectionMethod(),
                  imageRotation: description.sensorOrientation)
              .then(
            (dynamic result) async {
              setState(() {
                _barcodes = result;
              });
              if (result.length == 0) {
                // if the length of the photo is zero, so we set the fields of
                // _barcodeFound and _multipleBarcodesto false
                _barcodeFound = false;
                _multipleBarcodes = false;
              } else if (result.length > 1) {
                // is the length is more than one, then it has been detected more than one face
                _multipleBarcodes = true;
              } else {
                // otherwise one face has been detected
                _barcodeFound =
                    true; // the button now is available, and the person can take a barcode
              }
              if (_barcodeFound && !_multipleBarcodes) {
                Barcode _barcode;

                for (_barcode in result) {
                  // in that way we get all the photos with the same size
                  res = _barcode.rawValue; // returning the vectors to "res"

                }
              }
              setState(() {});
              _isDetecting = false;
            },
          ).catchError(
            (_) {
              _isDetecting = false;
            },
          );
        }
      });
    } catch (e) {
      log(e.toString());
    }
  }

  Future<List<dynamic>> Function(FirebaseVisionImage image)
      _getDetectionMethod() {
    final barcodeDetector = FirebaseVision.instance.barcodeDetector(
      BarcodeDetectorOptions(
        barcodeFormats: BarcodeFormat.all,
      ),
    );
    return barcodeDetector.detectInImage;
  }

  Widget _buildResults() {
    const Text noResultsText = const Text('');
    if (_barcodes == null || _camera == null || !_camera.value.isInitialized) {
      return noResultsText;
    }
    CustomPainter painter;

    final Size imageSize = Size(
      _camera.value.previewSize.height,
      _camera.value.previewSize.width,
    );
    painter = BarcodePainter(imageSize, _barcodes);
    return CustomPaint(
      painter: painter,
    );
  }

  Widget _buildImage() {
    if (_camera == null || !_camera.value.isInitialized) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }

    return Container(
      constraints: const BoxConstraints.expand(),
      child: _camera == null
          ? const Center(child: null)
          : Stack(
              fit: StackFit.expand,
              children: <Widget>[
                CameraPreview(_camera),
                _buildResults(),
              ],
            ),
    );
  }

  // in this method we deal with taking the photo and save it on firebase
  Future<void> exitScreen() async {
    setState(() {
      _buttonEnabled = false;
    });
    try {
      await _camera.stopImageStream();
      Map<String, dynamic> result = {
        "vector": res,
      };
      log(res);
      Navigator.of(context)
          .pop(result); // after the vector has been calculated,
      //we introduce the image in the register screen
    } catch (e) {
      log(e.toString());
      _buttonEnabled = true;
    }

    setState(() {
      _buttonEnabled = true;
    });
  }

  // taking the picture from the user and store it on firebase storage
  Future<String> takePicture() async {
    if (!_camera.value.isInitialized) {
      log('Error: select a camera first.');
      return null;
    }
    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/Pictures/profile_photo';
    await Directory(dirPath).create(recursive: true);
    final String filePath = '$dirPath/${uuid.v4()}.jpg';

    if (_camera.value.isTakingPicture) {
      // A capture is already pending, do nothing.
      return null;
    }

    try {
      await _camera.takePicture(filePath);
    } on CameraException catch (e) {
      log(e.toString());
      return null;
    }
    return filePath;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Search Your Product'),
      ),
      body: _buildImage(),
      floatingActionButton: FloatingActionButton(
        backgroundColor: (_barcodeFound) ? Colors.blue : Colors.blueGrey,
        child: Icon(Icons.camera),
        onPressed: _buttonEnabled
            ? () {
                try {
                  if (_barcodeFound) exitScreen();
                } catch (e) {
                  setState(() {
                    _buttonEnabled = true;
                  });
                }
              }
            : null,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  @override
  void dispose() {
    super.dispose();
    if (_camera != null) _camera?.dispose();
  }
}
