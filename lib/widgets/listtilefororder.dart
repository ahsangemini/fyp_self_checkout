import 'package:flutter/material.dart';

class OrderListTile extends StatefulWidget {
  OrderListTile(
      {Key key,
      @required this.image,
      @required this.name,
      @required this.price,
      @required this.quantity})
      : super(key: key);
  final String image, name;
  final double price;
  final int quantity;
  @override
  _OrderListTileState createState() => _OrderListTileState();
}

class _OrderListTileState extends State<OrderListTile> {
  bool _selected = false;
  FocusNode _quantityFocus = FocusNode();

  @override
  Widget build(BuildContext context) {
    return OrderListTile(
        widget.image, widget.name, widget.price, widget.quantity);
  }

  Widget OrderListTile(String image, String name, double price, int quantity) {
    return InkWell(
      onTap: () {
        FocusScope.of(context).requestFocus(_quantityFocus);
        setState(() {
          _selected = !_selected;
        });
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Flexible(
            child: Row(
              children: <Widget>[
                Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  margin: EdgeInsets.zero,
                  clipBehavior: Clip.antiAlias,
                  elevation: 10,
                  child: Container(
                    height: 80,
                    width: 80,
                    child: Image.network(image, fit: BoxFit.cover),
                  ),
                ),
                Flexible(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 12),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            name,
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                                fontWeight: FontWeight.bold),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            softWrap: false,
                          ),
                          Text(
                            "\$${price}",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                        ]),
                  ),
                ),
              ],
            ),
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Card(
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                  side: BorderSide(
                      color: _selected
                          ? Colors.grey
                          : Theme.of(context).primaryColor,
                      width: 2),
                ),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 12.0, vertical: 8),
                  child: Text(
                    quantity.toString(),
                    style: TextStyle(
                      color: _selected ? Colors.grey : Colors.black,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
