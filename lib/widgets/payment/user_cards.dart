import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:self_checkout_app/models/cart.dart';
import 'package:self_checkout_app/models/user.dart';
import 'package:self_checkout_app/utils/config.dart';
import 'package:self_checkout_app/utils/payments/cloud_service.dart';
import 'package:self_checkout_app/utils/payments/payment_service.dart';
import 'package:self_checkout_app/utils/payments/stripe_service.dart';
import 'package:stripe_payment/stripe_payment.dart';

class UserCardsPage extends StatefulWidget {
  final UserModel user;
  final bool isCheckout;
  final List<CartModel> cart;
  final double totalAmount, taxAmount, subTotal;

  const UserCardsPage(
      {Key key,
      @required this.user,
      this.isCheckout = false,
      @required this.cart,
      @required this.totalAmount,
      @required this.taxAmount,
      @required this.subTotal})
      : super(key: key);

  @override
  _UserCardsPageState createState() => _UserCardsPageState();
}

class _UserCardsPageState extends State<UserCardsPage> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    StripePayment.setOptions(StripeOptions(
        publishableKey: StripeConfig["publishableKey"],
        merchantId: StripeConfig["merchantId"],
        androidPayMode: StripeConfig["androidPayMode"]));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Please Select Card"),
        actions: [
          IconButton(
            onPressed: () => _addNewCard(),
            icon: Icon(Icons.credit_card, color: Colors.white),
          )
        ],
      ),
      body: SafeArea(
        child: _showUserCards(),
      ),
    );
  }

  Future<void> _addNewCard() async {
    try {
      log("Start of add new card");
      PaymentMethod paymentMethod =
          await StripePayment.paymentRequestWithCardForm(
              CardFormPaymentRequest());

      await PaymentService()
          .addCard(paymentMethod, widget.user.uid)
          .then((value) {
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text("Card added!"),
        ));
      }).catchError((error) {
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text(error.toString()),
        ));
      });
    } catch (e) {
      log(e.toString());
    }
  }

  _showUserCards() => StreamBuilder<List<PaymentMethod>>(
      stream: Firestore.instance
          .collection('users')
          .document(widget.user.uid)
          .collection('payment_methods')
          .snapshots()
          .map((querySnapshot) => querySnapshot.documents
              .map((document) => PaymentMethod.fromJson(document.data))
              .toList()),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          if (snapshot.data.length > 0) {
            return _buildListOfCards(snapshot.data);
          } else
            return Center(
              child: Text("You don`t have any cards"),
            );
        } else
          return Center(
            child: CircularProgressIndicator(),
          );
      });

  _buildListOfCards(List<PaymentMethod> methods) {
    return ListView.separated(
        itemBuilder: (context, index) => ListTile(
              onTap: () =>
                  widget.isCheckout ? _checkoutAndPop(methods[index]) : null,
              title: Text("**** ${methods[index].card.last4}"),
              subtitle: Text(
                  "Expires at: ${methods[index].card.expMonth}/${methods[index].card.expYear}"),
            ),
        separatorBuilder: (context, index) => Divider(
              color: Colors.black54,
            ),
        itemCount: methods.length);
  }

  Future<void> _checkoutAndPop(PaymentMethod paymentMethod) async {
    await _checkout(paymentMethod);
  }

  Future<void> _checkout(PaymentMethod paymentMethod) async {
    if (paymentMethod == null) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Choose a credit card first!"),
      ));
    } else {
      CloudService.createPaymentIntent(
        cart: widget.cart,
        totalAmount: widget.totalAmount * 100,
        taxAmount: widget.taxAmount,
        subTotal: widget.subTotal,
        currency: 'usd',
        customer: widget.user.customer_id,
      ).then((response) async {
        if (await confirmDialog(
            response.data["client_secret"], paymentMethod)) {
          Navigator.of(context).pop();
        }
        ;
      }).catchError((err) {
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text("Something went wrong! Try again"),
        ));
      });
    }
  }

  Future<bool> confirmDialog(String sec, PaymentMethod paymentMethod) async {
    var confirm = AlertDialog(
      title: Text("Confirm Payment"),
      content: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              "Confirm payment with card **** ${paymentMethod.card.last4}",
              // style: TextStyle(fontSize: 25),
            ),
            Text("Charge amount: ${widget.totalAmount * 100}USD")
          ],
        ),
      ),
      actions: <Widget>[
        new RaisedButton(
          child: new Text('CANCEL'),
          onPressed: () {
            Navigator.of(context).pop(false);
            final snackBar = SnackBar(
              content: Text('Payment Cancelled'),
            );
            _scaffoldKey.currentState.showSnackBar(snackBar);
          },
        ),
        new RaisedButton(
          child: new Text('Confirm'),
          onPressed: () {
            Navigator.of(context).pop(true);
            _confirmPayment(sec, paymentMethod); // function to confirm Payment
          },
        ),
      ],
    );
    return await showDialog<bool>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return confirm;
        });
  }

  _confirmPayment(String sec, PaymentMethod paymentMethod) async {
    var response = await StripeService.confirmPaymentIntent(sec, paymentMethod);
    if (response.success) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Completed!"),
      ));
      print(
          "last4: ${paymentMethod.card.last4}, payment method id: ${paymentMethod.id}");
    } else
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Failed to pay!"),
      ));
  }
}
