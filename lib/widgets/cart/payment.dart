import 'dart:developer';

import 'package:cloud_functions/cloud_functions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:self_checkout_app/bloc/form_bloc/form_bloc_bloc.dart';
import 'package:self_checkout_app/bloc/orders_bloc/bloc.dart';
import 'package:self_checkout_app/bloc/user_bloc/bloc.dart';
import 'package:self_checkout_app/models/cart.dart';
import 'package:self_checkout_app/models/user.dart';
import 'package:self_checkout_app/utils/config.dart';
import 'package:self_checkout_app/utils/payments/stripe.dart';
import 'package:self_checkout_app/widgets/custom_route.dart';
import 'package:self_checkout_app/widgets/orders/orders.dart';
import 'package:self_checkout_app/widgets/payment/user_cards.dart';
import 'package:self_checkout_app/widgets/progress.dart';

class Payment extends StatefulWidget {
  final List<CartModel> cart;
  final double totalAmount;
  final double taxAmount;
  final double subTotal;
  final UserModel user;

  Payment(
      {@required this.cart,
      @required this.totalAmount,
      @required this.user,
      @required this.subTotal,
      @required this.taxAmount});

  @override
  _PaymentState createState() => _PaymentState();
}

class _PaymentState extends State<Payment> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();
  bool _saving = false;

  // Verify payment for order, if failed then give user an option to retry.

  // Create an order
  onFinishStripe(data) {
    setState(() {
      _saving = true;
    });
    Navigator.pop(context);
    // Verify the order details and then set savings status
    print("FUNC onFinisges ${data.status}");
    if (data.status == "succeeded") {
      print("coming here");
      Navigator.of(context).pushReplacement(
        FadeRoute(
          page: BlocProvider(
            create: (context) =>
                OrdersBloc()..add(LoadOrders(userId: widget.user.uid)),
            child: Orders(
              user: widget.user,
            ),
          ),
        ),
      );
      setState(() {
        _saving = false;
      });
    }
  }

  @override

  /// For radio button
  int tapvalue = 0;
  int tapvalue2 = 0;

  Widget build(BuildContext context) {
    if (widget.cart.length == 0) {
      Navigator.pop(context);
      return null;
    }

    print(tapvalue2);
    print(tapvalue);
    return ModalProgressHUD(
      inAsyncCall: _saving,
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Theme.of(context).cardColor,
        appBar: AppBar(
          leading: InkWell(
              onTap: () {
                Navigator.of(context).pop(false);
              },
              child: Icon(Icons.arrow_back_ios)),
          elevation: 0.0,
          title: Text(
            "Payment",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 22.0,
              color: Colors.white,
            ),
          ),
          centerTitle: true,
          iconTheme: IconThemeData(color: Colors.white),
        ),
        body: SingleChildScrollView(
          physics: ClampingScrollPhysics(),
          child: Container(
            child: Padding(
              padding:
                  const EdgeInsets.only(top: 30.0, left: 20.0, right: 20.0),
              child: Column(
                children: <Widget>[
                  Padding(padding: EdgeInsets.only(top: 22.0, bottom: 12)),
                  Text(
                    "Choose your Payment method",
                    style: TextStyle(
                      letterSpacing: 0.1,
                      fontWeight: FontWeight.w600,
                      fontSize: 25.0,
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(top: 60.0)),

                  InkWell(
                    onTap: () {
                      setState(() {
                        if (tapvalue == 0) {
                          tapvalue++;
                          tapvalue2 = 0;
                        } else {
                          tapvalue--;
                        }
                      });
                    },
                    child: Row(
                      children: <Widget>[
                        Radio(
                          value: 1,
                          groupValue: tapvalue,
                          onChanged: null,
                        ),
                        Text(
                          "Credit / Debit Card",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 17.0),
                        ),
                      ],
                    ),
                  ),

                  Padding(padding: EdgeInsets.only(top: 15.0)),
                  Divider(
                    height: 1.0,
                    color: Colors.black26,
                  ),
                  Padding(padding: EdgeInsets.only(top: 15.0)),
                  InkWell(
                    onTap: () {
                      setState(() {
                        if (tapvalue2 == 0) {
                          tapvalue2++;
                          tapvalue = 0;
                        } else {
                          tapvalue2--;
                        }
                      });
                    },
                    child: Row(
                      children: <Widget>[
                        Radio(
                          value: 1,
                          groupValue: tapvalue2,
                          onChanged: null,
                        ),
                        Text("Pay at the Counter",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 17.0)),
                      ],
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(top: 15.0)),
                  Divider(
                    height: 1.0,
                  ),

                  Padding(padding: EdgeInsets.only(top: 110.0)),

                  /// Button pay
                  InkWell(
                    onTap: () async {
                      if (tapvalue == 1) {
                        if (PaymentProviderEnabled["name"] == "stripe") {
                          Navigator.of(context).push(
                            SlideTopRoute(
                              page: UserCardsPage(
                                user: widget.user,
                                cart: widget.cart,
                                subTotal: widget.subTotal,
                                taxAmount: widget.taxAmount,
                                totalAmount: widget.totalAmount,
                                isCheckout: true,
                              ),
                            ),
                          );
                        }
                      } else if (tapvalue2 == 1) {
                        print("came 2");
                        try {
                          setState(() {
                            _saving = true;
                          });
                          final HttpsCallable callable =
                              CloudFunctions.instance.getHttpsCallable(
                            functionName: 'createCOCOrder',
                          );
                          final HttpsCallableResult resp =
                              await callable.call(<String, dynamic>{
                            'cart': widget.cart.map((c) => c.toMap()).toList(),
                            'totalAmount': widget.totalAmount,
                            'taxAmount': widget.taxAmount,
                            'subTotal': widget.subTotal
                          });

                          Navigator.of(context).pushReplacement(
                            FadeRoute(
                              page: BlocProvider(
                                create: (context) => OrdersBloc()
                                  ..add(LoadOrders(userId: widget.user.uid)),
                                child: Orders(
                                  user: widget.user,
                                ),
                              ),
                            ),
                          );
                        } catch (e) {
                          setState(() {
                            _saving = false;
                          });
                          log(e.toString());
                          Fluttertoast.showToast(
                              msg:
                                  "Something went wrong please try again later",
                              toastLength: Toast.LENGTH_LONG,
                              gravity: ToastGravity.CENTER,
                              backgroundColor: Colors.red,
                              textColor: Colors.white,
                              fontSize: 16.0);
                        }
                      }
                    },
                    child: Container(
                      height: 55.0,
                      width: 300.0,
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                      ),
                      child: Center(
                        child: Text(
                          tapvalue2 == 1 ? "Confirm" : "Pay",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w700,
                              fontSize: 16.5,
                              letterSpacing: 2.0),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
