import 'package:flutter/material.dart';
import 'package:self_checkout_app/utils/config.dart';

class AboutUs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "About SelfCheckout",
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 15.0,
            color: Colors.white,
          ),
        ),
        centerTitle: true,
        iconTheme: IconThemeData(color: Color(0xFF6991C7)),
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Container(
          color: Colors.white,
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Divider(
                  height: 0.5,
                  color: Colors.black12,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10.0, left: 15.0),
                child: Center(
                  child: Container(
                      child: Text(
                    "Ahsan 62715",
                    textAlign: TextAlign.center,
                    style: new TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 0.73,
                      fontSize: 24,
                    ),
                  )),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Divider(
                  height: 0.5,
                  color: Colors.black12,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Text(
                  "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the "
                  "industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and "
                  "scrambled it to make a type specimen book. \n\n\n It has survived not only five centuries, but also "
                  "the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the "
                  "1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with "
                  "desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                  style: TextStyle(
                    color: Colors.black38,
                    fontSize: 15.0,
                    fontWeight: FontWeight.w500,
                  ),
                  textAlign: TextAlign.justify,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
