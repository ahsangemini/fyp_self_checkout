import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:self_checkout_app/bloc/orders_bloc/bloc.dart';
import 'package:self_checkout_app/models/cart.dart';
import 'package:self_checkout_app/models/order.dart';
import 'package:self_checkout_app/models/user.dart';
import 'package:self_checkout_app/screens/barcodescreen.dart';
import 'package:self_checkout_app/widgets/custom_route.dart';
import 'package:self_checkout_app/widgets/orders/order_details.dart';

class Orders extends StatelessWidget {
  final UserModel user;
  const Orders({Key key, @required this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "My Orders",
            style: TextStyle(
              fontSize: 22.0,
              color: Colors.white,
            ),
          ),
          centerTitle: true,
          iconTheme: IconThemeData(color: Colors.white),
          elevation: 0.0,
        ),
        body: BlocBuilder(
          bloc: BlocProvider.of<OrdersBloc>(context),
          builder: (BuildContext context, OrdersState state) {
            if (state is OrdersLoading) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            if (state is OrdersLoaded) {
              return StreamBuilder(
                stream: state.orders,
                initialData: [],
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (snapshot.hasError) {
                    return Container(
                      child: Center(
                        child: Text("Something went wrong"),
                      ),
                    );
                  }
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting:
                      return Container(
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      );
                      break;
                    default:
                      final List<OrderModel> orders = snapshot.data;
                      print(orders.length);
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ListView.builder(
                          itemCount: orders.length,
                          itemBuilder: (BuildContext context, int index) {
                            final OrderModel order = orders[index];
                            final List<CartModel> carts = order.cart;

                            return Card(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              carts.length > 1
                                                  ? Text(
                                                      "${carts[0].product.name} + ${carts.length - 1} items")
                                                  : Text(
                                                      "${carts[0].product.name}"),
                                              Container(
                                                height: 100,
                                                child: ListView.builder(
                                                  itemCount: carts.length,
                                                  scrollDirection:
                                                      Axis.horizontal,
                                                  itemBuilder:
                                                      (BuildContext context,
                                                          int index) {
                                                    return Container(
                                                        padding:
                                                            EdgeInsets.all(8.0),
                                                        child: Image.network(
                                                            carts[index]
                                                                .product
                                                                .images[0]));
                                                  },
                                                ),
                                              ),
                                              Padding(
                                                  padding: EdgeInsets.only(
                                                      top: 7.0)),
                                            ],
                                          ),
                                        ),
                                        Expanded(
                                          child: Column(
                                            children: [
                                              RaisedButton(
                                                color: Theme.of(context)
                                                    .primaryColor,
                                                child: Text(
                                                  "Details",
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                ),
                                                onPressed: () {
                                                  Navigator.of(context).push(
                                                    SlideTopRoute(
                                                        page: OrderDetails(
                                                      order: order,
                                                    )),
                                                  );
                                                },
                                              ),
                                              RaisedButton(
                                                color: Theme.of(context)
                                                    .primaryColor,
                                                child: Text(
                                                  "Generate Barcode",
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                ),
                                                onPressed: () {
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              BarCodePage(
                                                                data: order
                                                                    .orderId,
                                                              )));
                                                },
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "Date - ${order.createdAt.toIso8601String().substring(0, 10)}",
                                              style: TextStyle(
                                                color: Colors.green,
                                                fontSize: 15.0,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                            Padding(
                                                padding:
                                                    EdgeInsets.only(top: 7.0)),
                                            Text(
                                              "ID: #${order.orderId}",
                                              style: TextStyle(
                                                fontSize: 15.0,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            Padding(
                                                padding:
                                                    EdgeInsets.only(top: 7.0)),
                                            Text(
                                              "Status: ${order.status2 != null ? order.status2 : order.status3 != null ? order.status3 : order.status}",
                                              style: TextStyle(
                                                fontSize: 15.0,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      );
                  }
                },
              );
            }
            return Container();
          },
        ));
  }
}
