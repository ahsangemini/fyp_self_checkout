import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:self_checkout_app/authentication_bloc/authentication_bloc.dart';
import 'package:self_checkout_app/authentication_bloc/bloc.dart';
import 'package:self_checkout_app/bloc/locale/bloc.dart';
import 'package:self_checkout_app/bloc/theme/bloc.dart';
import 'package:self_checkout_app/bloc_delegate.dart';
import 'package:self_checkout_app/models/user.dart';
import 'package:self_checkout_app/screens/auth.dart';
import 'package:self_checkout_app/screens/home.dart';
import 'package:self_checkout_app/screens/intro.dart';
import 'package:self_checkout_app/screens/splash.dart';
import 'package:self_checkout_app/services/user_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bloc/bloc.dart';
import 'package:self_checkout_app/utils/config.dart';
import 'package:self_checkout_app/utils/styles.dart';
import 'package:self_checkout_app/utils/tools.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

import 'login/bloc/bloc.dart';
import 'widgets/app_localizations.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await allTranslations.init();

  BlocSupervisor.delegate = AppBlocDelegate();
  final UserRepository userRepository = UserRepository();
  WidgetsFlutterBinding.ensureInitialized();
  final FirebaseMessaging firebaseMessaging = FirebaseMessaging();
  firebaseMessaging.subscribeToTopic("main");
  firebaseMessaging.requestNotificationPermissions(
    const IosNotificationSettings(
        sound: true, badge: true, alert: true, provisional: false),
  );
  var token = await firebaseMessaging.getToken();
  print(token.toString() + " This Is The Token");
  firebaseMessaging.configure(
    onMessage: (Map<String, dynamic> message) async {
      print("onMessage: $message");
      // _showItemDialog(message);
    },
    onBackgroundMessage: myBackgroundMessageHandler,
    onLaunch: (Map<String, dynamic> message) async {
      print("onLaunch: $message");
      // _navigateToItemDetail(message);
    },
    onResume: (Map<String, dynamic> message) async {
      print("onResume: $message");
      // _navigateToItemDetail(message);
    },
  );

  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider<AuthenticationBloc>(
          create: (context) =>
              AuthenticationBloc(userRepository: userRepository)
                ..add(AppStarted()),
        ),
      ],
      child: App(
        userRepository: userRepository,
      ),
    ),
  );
}

class App extends StatefulWidget {
  final UserRepository _userRepository;
  final String theme;
  const App(
      {Key key, @required UserRepository userRepository, this.theme = "light"})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);
  @override
  State<StatefulWidget> createState() {
    return AppState();
  }
}

class AppState extends State<App> {
  bool isFirstSeen = false;

  @override
  void initState() {
    super.initState();
    checkFirstSeen();

    // Initializes a callback should something need
    // to be done when the language is changed
    allTranslations.onLocaleChangedCallback = _onLocaleChanged;
  }

  ///
  /// If there is anything special to do when the user changes the language
  ///
  _onLocaleChanged() async {
    // do anything you need to do if the language changes
    print('Language has been changed to: ${allTranslations.currentLanguage}');
  }

  Future checkFirstSeen() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool _seen = (prefs.getBool('seen') ?? false);
    // prefs.setBool('seen', false);
    if (_seen)
      setState(() {
        isFirstSeen = true;
      });
    else {
      setState(() {
        isFirstSeen = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => LocaleBloc(),
      child: BlocBuilder<LocaleBloc, LocaleState>(
        builder: (BuildContext context, LocaleState localeState) {
          return BlocProvider(
            create: (context) =>
                ThemeBloc()..add(ThemeChanged(type: widget.theme)),
            child: BlocBuilder<ThemeBloc, ThemeState>(
              builder: (BuildContext context, ThemeState state) {
                return MaterialApp(
                  debugShowCheckedModeBanner: false,
                  theme: state.type == "dark"
                      ? buildDarkTheme().copyWith(
                          primaryColor:
                              HexColor(Settings["Setting"]["MainDarkColor"]),
                          buttonColor: HexColor(
                              Settings["Setting"]["SecondaryDarkColor"]),
                        )
                      : buildLightTheme(
                              HexColor(Settings["Setting"]["MainColor"]))
                          .copyWith(
                          primaryColor:
                              HexColor(Settings["Setting"]["MainColor"]),
                          buttonColor:
                              HexColor(Settings["Setting"]["SecondaryColor"]),
                        ),

                  locale: localeState is LocaleChangeFailed
                      ? Locale("en")
                      : localeState.locale,
                  localizationsDelegates: [
                    GlobalMaterialLocalizations.delegate,
                    GlobalWidgetsLocalizations.delegate,
                    GlobalCupertinoLocalizations.delegate,
                  ],
                  // Tells the system which are the supported languages
                  supportedLocales: allTranslations.supportedLocales(),

                  home: !isFirstSeen
                      ? IntroScreen()
                      : BlocBuilder(
                          bloc: BlocProvider.of<AuthenticationBloc>(context),
                          builder: (BuildContext context,
                              AuthenticationState state) {
                            if (state is Uninitialized) {
                              return SplashCreen();
                            }
                            if (state is Unauthenticated) {
                              return BlocProvider<LoginBloc>(
                                  create: (context) => LoginBloc(
                                      userRepository: widget._userRepository),
                                  child: AuthScreen(
                                      userRepository: widget._userRepository));
                            }

                            if (state is Authenticated) {
                              print(state.user.uid.toString() +
                                  "This is the user id of");
                              return StreamBuilder<UserModel>(
                                stream:
                                    UserRepository().getUser(state.user.uid),
                                builder: (BuildContext context,
                                    AsyncSnapshot<UserModel> snapshot) {
                                  if (snapshot.hasError) {
                                    return SplashCreen();
                                  } else if (snapshot.connectionState ==
                                      ConnectionState.waiting) {
                                    return SplashCreen();
                                  } else {
                                    return Home(
                                      user: snapshot.data == null
                                          ? state.user
                                          : snapshot.data,
                                    );
                                  }
                                },
                              );
                            }

                            return SplashCreen();
                          },
                        ),
                );
              },
            ),
          );
        },
      ),
    );
  }
}

Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) async {
  if (message.containsKey('data')) {
    // Handle data message
    final dynamic data = message['data'];
  }
  if (message.containsKey('notification')) {
    // Handle notification message
    final dynamic notification = message['notification'];
  }
  // Or do other work.
}
